from dataModel.models import *
from core.program import *
from .FlaskMethod import FlaskMethod

#==========================================================

class FlaskNode(Object):
    
#==========================================================
    def onTestRequest(self,request):
        return True

    def onProcessRequest(self,request):
        for method in self.children.by_class(FlaskMethod):
            if method.onTestRequest(request):
                method.onProcessRequest(request)
                if request.done==True:break
#==========================================================
