from .node import *
from mod_program import *
#============================================================

class Api_Argument(Api_Element):

#============================================================

    CLS=String()
    #-------------------------------------------------------
    def test(self,request):
    #-------------------------------------------------------
        return True

    #-------------------------------------------------------
    def get_help(self):
    #-------------------------------------------------------
        yield "name",self.name
        yield "cls",self.cls
    #-------------------------------------------------------

#============================================================

class Api_Method(Api_Element):

#============================================================

    REDIRECT=String(default=None)
    #-------------------------------------------------------
    def get_mime(self):
    #-------------------------------------------------------
        return "application/json"

    #-------------------------------------------------------
    def test(self,request):
    #-------------------------------------------------------
        for child in self.children.by_class(Api_Argument):
            if child.test(request)!=True:
                return False
        return True

    #-------------------------------------------------------
    def process_request(self,request,**args):
    #-------------------------------------------------------
        print(args)
        if self.test(request)==True:
            r= self.onResponse(request,**args)
        if self.redirect is not None:
            return self.redirection(self.redirect)
        else:
            return r

    #-------------------------------------------------------
    def onResponse(self,request,**args):
    #-------------------------------------------------------
        return {}

    #-------------------------------------------------------
    def get_help(self):
    #-------------------------------------------------------
        for elt in self.children.by_class(Api_Argument):
            yield dict(elt.get_help())
#============================================================

class Api_Request(Code_Block):

#============================================================
    """
    un noeud du serveur est une url accessible et programmable
    qui effectue une action suivant la requete
    object basic d'un serveur

    comportement par defaut de flask
    si text : renvoie text
    si dict, list : renvoie json
    """


    #-------------------------------------------------------
    def get_url(self,node):

        return "/"+node.path()

    #-------------------------------------------------------
    def get_server(self,node):
        if hasattr(node,"app"):
            return node
        for elt in node.ancestors:
            if hasattr(elt,"app"):
                return elt

    #-------------------------------------------------------
    def onCleanArgs(self,**args):

        for k,v in args.items():
            if type(v)==list:
                if len(v)==1:
                    yield k,v[0]
                else:
                    yield k,v
            else:
                yield k,v

    #-------------------------------------------------------
    def process(self,node,**args):
        print("request",self.path(),node.path())
        #node.show()
        self.node=node
        self.server=self.get_server(node)
        self.url_args=args
        self.args=dict(self.onCleanArgs(**request.args))
        self.request=request
        self.method=request.method

        args.update(self.args)

        for mod in self.server.children.by_class("Api_Module"):
            if mod.process(self) == False:
                return mod.error("module fail",mod.name)
        #print(node.path())
        return self.node.onResponse(self,node=node,**args)

    #-------------------------------------------------------




#============================================================

class Api_Help(Api_Method):

#============================================================
    SELECT=String()
    #-------------------------------------------------------
    def onSetup(self):
    #-------------------------------------------------------
        self.selected_node=self.parent.find(self.select)

    #-------------------------------------------------------
    def onResponse(self,request,**args):
    #-------------------------------------------------------
        r=dict()
        for elt in self.selected_node.all().by_class(Api_Node):
            r["/"+elt.path()]=dict(elt.get_help())
        return r
    #-------------------------------------------------------

#============================================================

class Api_Node(Api_Element):

#============================================================
    """
    un noeud du serveur est une url accessible et programmable
    qui effectue une action suivant la requete
    object basic d'un serveur

    comportement par defaut de flask
    si text : renvoie text
    si dict, list : renvoie json

    ['GET','POST','PUT','DELETE']
    """
    def onSetup(self):

        self.server_path=self.get_url()

    #-------------------------------------------------------
    def get_url(self):
    #-------------------------------------------------------
        url=""
        ok=True
        parent=self
        while ok:
            if isinstance(parent,Api_Node) and isinstance(parent.parent,Api_Node):
                url="/"+parent.name+url
                parent=parent.parent
            else:
                ok=False

        if url=="":
            url="/"
        return url

    #-------------------------------------------------------
    def get_methods(self):
    #-------------------------------------------------------

        return [elt.name for elt in self.children.by_class("Api_Method")]

    #-------------------------------------------------------
    def get_method(self,name):
    #-------------------------------------------------------

        for elt in self.children.by_class("Api_Method"):
            if elt.name==name:
                return elt


    #-------------------------------------------------------
    def map(self,app):
    #-------------------------------------------------------


        url=self.get_url()+"/"
        self.add_rule(app,self.path().replace('/','_')+"_main",
                        url, self.process_request,self.get_methods())   

    #-------------------------------------------------------
    def add_rule(self,app,name,path,func,methods):
    #-------------------------------------------------------

            app.add_url_rule(path, 
                             name, 
                             func,methods=methods)
    
    #-------------------------------------------------------
    def process_request(self,**args):
    #-------------------------------------------------------

        current_request=Api_Request()
        return current_request.process(self,**args)

    #-------------------------------------------------------
    def onResponse(self,request,**args):
    #-------------------------------------------------------

        method=self.get_method(request.method)
        if method is None:
            return self.error(text='no method',method=request.method)

        return method.process_request(request=request,**args)


    #-------------------------------------------------------
    def get_mime(self):
    #-------------------------------------------------------
        return "text/html"

    #-------------------------------------------------------
    def get_help(self):
    #-------------------------------------------------------

        for elt in self.children.by_class(Api_Method):
            yield elt.name,list(elt.get_help())

    #-------------------------------------------------------
#============================================================

class Api_Path(Api_Node):

#============================================================
    #-------------------------------------------------------
    def map(self,app):
    #-------------------------------------------------------

        if self.parent.parent:
            url="/"+self.parent.path()+"/<path:path>"
        else:
            url="/<path:path>"


        self.add_rule(app,self.path().replace('/','_'),
                        url, self.process_request,self.get_methods()) 

    #-------------------------------------------------------
    def onResponse(self,request,**args):
    #-------------------------------------------------------
        method=self.get_method(request.method)
        if method is None:
            return self.error(text='no method',method=request.method)

        return method.process_request(request=request,**args)

    #-------------------------------------------------------


#============================================================
