

from flask import send_from_directory
from dataModel import *
from ..node import *
from ..api import *
import os

#============================================================

class Api_StaticFile(Api_Node):

#============================================================
    DEFAULT_METHODS=["GET"]
    FILE_PATH=String()
    #-------------------------------------------------------
    def onSetup(self,**args):

        root=False
        if self.file_path.startswith("/"):
            root=True

        lst=self.file_path.split("/")

        newlist=[]
        for elt in lst:
            if elt.strip() !="":
                newlist.append(elt)

        self.__path="/"+os.path.join(*newlist[:-1])
        if not root:
            self.__path=self.__path[1:]

        self.__filename=newlist[-1]
        #print(path,filename)
    #-------------------------------------------------------
    def map(self,app):
    #-------------------------------------------------------

        if self.parent:
            url=self.get_url()+"/"
        else:
            url="/"


        self.add_rule(app,self.path().replace('/','_')+"_static",
                        url, self.process_request,["GET"]) 
  
    #-------------------------------------------------------
    def onResponse(self,request,**args):

        print(self.__path,self.__filename,os.path.exists(self.file_path))
        return send_from_directory(self.__path,self.__filename)

    #-------------------------------------------------------

#============================================================

class Api_StaticDir(Api_Node):

#============================================================

    FILE_PATH=String()
    #-------------------------------------------------------
    def map(self,app):
    #-------------------------------------------------------

        if self.parent:
            url=self.get_url()+"/"
        else:
            url="/"

        self.add_rule(app,self.path().replace('/','_')+"_main",
                        url, self.process_request,["GET"]) 
  

        self.add_rule(app,self.path().replace('/','_')+"_static",
                        url+"<path:path>", self.process_request,["GET"]) 
  
    #-------------------------------------------------------
    def onResponse(self,request,path=None,**args):

        if path:
            url_path="/"+self.path()+"/"+path+"/"
            fullpath=os.path.join(self.file_path,path)

        elif os.path.exists(self.file_path+"/index.html"):
            url_path="/"+self.path()
            fullpath=self.file_path+"/index.html"
            path="index.html"
        else:
            url_path="/"+self.path()+"/"
            fullpath=self.file_path
            path=""

        if os.path.isfile(fullpath):
            return send_from_directory(self.file_path,path)

        elif os.path.isdir(fullpath):
            r=[]
            for elt in os.listdir(fullpath):
                r.append(url_path+elt)
            return dict(result=r)

    #-------------------------------------------------------------    
    def has_element(self,*args):
        print(self.file_path+os.path.join(self.file_path,*args))
        return os.path.exists(self.file_path+os.path.join(*args))

    #-------------------------------------------------------

#============================================================


