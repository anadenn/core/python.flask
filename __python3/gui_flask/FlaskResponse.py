from dataModel.models import *
from core.program import *

#===================================================================

class FlaskResponse(Object):

#===================================================================
    MIMETYPE=String(default='text')

    def onTestRequest(self,request):
        return True

    def onProcessRequest(self,request):
        request.response = Response("", mimetype=self.mimetype)
    #-------------------------------------------------------

#===================================================================

class FlaskXmlResponse(FlaskResponse):

#===================================================================

    def onTestRequestX(self,request):
        return True

    def onProcessRequestX(self,request):
        request.response = Response(tree_to_string(request), mimetype=self.mimetype)
    #-------------------------------------------------------
#===================================================================

class FlaskRedirectResponse(FlaskResponse):

#===================================================================
    REDIRECT=String(default='/')

    def onTestRequest(self,request):
        return True

    def onProcessRequest(self,request):
        request.response = redirect(self.redirect)
    #-------------------------------------------------------


#===================================================================
