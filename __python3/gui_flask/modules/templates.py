from dataModel.models import *
from core.program import *
from ..FlaskResponse import FlaskResponse
from flask import Response
from flask import render_template

#===================================================================

class FlaskTemplateResponse(FlaskResponse):

#===================================================================
    TEMPLATE=String(default='index.html')

    def onTestRequest(self,request):
        return True

    def onProcessRequest(self,request):
        request.response = Response(render_template(self.template,node=request), mimetype=self.mimetype)
    #-------------------------------------------------------
#===================================================================
